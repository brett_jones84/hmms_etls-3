--look up drop table syntax - check to see if table exists , if yes then drop and proceed with creation --
Create table ServiceRequest (
	ServiceRequestID int IDENTITY(1,1) primary key,	
	ServiceRequest_VW_ID int, 
	[Status] varchar(50),	
	TimeStampSent datetime2,	 
	TimeStampReceived datetime2
)	

GO
--look up drop table syntax - check to see if table exists , if yes then drop and proceed with creation --
Create table Comment (
	ID int IDENTITY(1,1) primary key
	,ServiceRequestID int
	,WorkOrderID int
	,CommentText varchar(max)	
	, CommentType varchar(50)
	,CommentCreationDate datetime2	
	,CommentUpdateDate datetime2	
)