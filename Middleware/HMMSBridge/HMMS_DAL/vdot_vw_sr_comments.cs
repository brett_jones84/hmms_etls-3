//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HMMS_DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class vdot_vw_sr_comments
    {
        public string CSC_SR_ID { get; set; }
        public Nullable<int> WorkOrderID { get; set; }
        public string CommentType { get; set; }
        public string Comment { get; set; }
        public Nullable<System.DateTime> CommentCreationDate { get; set; }
        public Nullable<System.DateTime> CommentUpdateDate { get; set; }
        public Nullable<int> HMMS_SR_ID { get; set; }
        public int ID { get; set; }
    }
}
