﻿var ViewModel = function () {
    var self = this;

    //Rest Endpoints
    var getVdot_servicerequest_URI = 'api/CSC/servicerequests';
    var postAddVueWorksIDToServiceRequest_URI = 'api/CSC/servicerequests/updatehmmsid/{CSC_SR_ID}?HMMS_SR_ID={HMMS_SR_ID}';
    var postUpdateServiceRequestStatus_URI = 'api/CSC/servicerequests/updatestatus/{CSC_SR_ID}?CSC_SR_Status={CSC_SR_Status}';
    var postCSC_Comments_URI = 'api/CSC/servicerequests/Comment';
    var getHMMSComments_URI = 'api/HMMS/servicerequests/Comments';
    var getCSCComments_URI = 'api/CSC/servicerequests/Comments';
    var getSr_closures_URI = 'api/HMMS/servicerequests/closures';
    var getSr_cancels_csc_URI = 'api/CSC/servicerequests/pendingcancels';
    var get_wo_to_customer_URI = 'api/HMMS/workorders/toCustomer';
    
    self.statusCodes = ko.observableArray(['Not Applicable', 'Needs Review', 'Assigned', 'In Process', 'Cancel Requested', 'Cancelled', 'Pending', 'Rejected', 'Closed', 'Inactive']);
    
    self.comments_csc = ko.observableArray();
    self.comments_hmms = ko.observableArray();
    self.sr_closures_hmms = ko.observableArray();
    self.serviceRequests = ko.observableArray();
    self.sr_cancels_csc = ko.observableArray();
    self.wo_to_customer = ko.observableArray();

    

    self.error = ko.observable();
    self.success = ko.observable();
    self.info = ko.observable();
    self.detail = ko.observable();
    self.detailSR = ko.observable();

    self.newComment = {
        CSC_SR_ID: ko.observable(116136),
        CommentText: ko
            .observable("A new test at: " + new Date(Date.now()).toISOString()),
        CommentSubject: ko.observable("This is a test Subject"),
        TimeStamp: ko.observable(new Date(Date.now()).toISOString()),
        useDBForCompare: ko.observable(false)

    };

    self.newServiceRequest = {
        CSC_SR_ID: ko.observable("123"),
        HMMS_SR_ID: ko.observable("456"),
        Status: ko.observable("GOOD"),
        TimeStampSent: ko.observable(new Date(Date.now()).toISOString()),
        TimeStampReceived: ko.observable(new Date(Date.now() + 1000).toISOString())
    };

    self.updatedServiceRequest = {
        CSC_SR_ID: ko.observable(116136),
        HMMS_SR_ID: ko.observable(),
        Status: ko.observable(),
        TimeStampSent: ko.observable(),
        TimeStampReceived: ko.observable(),
        CommentText: ko.observable()
    };

    self.AddVueWorksIDToServiceRequest = function (item) {
        var uri = postAddVueWorksIDToServiceRequest_URI;
        uri = uri.replace("{CSC_SR_ID}", self.updatedServiceRequest.CSC_SR_ID());
        uri = uri.replace("{HMMS_SR_ID}", self.updatedServiceRequest.HMMS_SR_ID());
        ajaxHelper(uri, 'POST')
            .done(function (item) {
                console.log("Success");
            });
    };

    self.UpdateServiceRequestStatus = function (item) {
        var uri = postUpdateServiceRequestStatus_URI;
        uri = uri.replace("{CSC_SR_ID}", self.updatedServiceRequest.CSC_SR_ID());
        uri = uri.replace("{CSC_SR_Status}", self.updatedServiceRequest.Status());
        ajaxHelper(uri, 'POST')
            .done(function (item) {
                //self.serviceRequests.push(item);
                console.log("Success");
                //getServiceRequests();
            });
    };

    self.AddCommentToServiceRequest = function (item) {

        var uri = postCSC_Comments_URI;

        var comment = {
            CSC_SR_ID: self.newComment.CSC_SR_ID(),
            CommentText: self.newComment.CommentText(),
            CommentSubject: self.newComment.CommentSubject(),
            TimeStamp: self.newComment.TimeStamp(),
            useDBForCompare: self.newComment.useDBForCompare()
        };

        ajaxHelper(uri, 'POST', comment)
            .done(function (item) {
                //self.serviceRequests.push(item);
                console.log("Success");
                //getServiceRequests();
            });
    };

    self.getvdot_servicerequest = function() {
        ajaxHelper(getVdot_servicerequest_URI, 'GET')
            .done(function(data) {
                self.serviceRequests(data);
            });
    };

    self.getCSCComments = function() {
        ajaxHelper(getCSCComments_URI, 'GET')
            .done(function(data) {
                self.comments_csc(data);
            });
    };

    self.getHMMSComments = function() {
        ajaxHelper(getHMMSComments_URI, 'GET')
            .done(function(data) {
                self.comments_hmms(data);
            });
    };

    self.getSr_closures = function() {
        ajaxHelper(getSr_closures_URI, 'GET')
            .done(function(data) {
                self.sr_closures_hmms(data);
            });
    };

    self.getSr_cancels_csc = function() {
        ajaxHelper(getSr_cancels_csc_URI, 'GET')
            .done(function(data) {
                self.sr_cancels_csc(data);
            });
    };


    self.get_wo_to_customer = function () {
        ajaxHelper(get_wo_to_customer_URI, 'GET')
            .done(function (data) {
                debugger;
                self.wo_to_customer(data);
            });
    };


    function ajaxHelper(uri, method, data) {
        self.error(''); // Clear error message
        self.success('');
        self.info('Waiting for Data from: URL: '+uri + ', method:  ' + method +', data: ' + data);
        return $.ajax({
            type: method,
            url: uri,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null,
            success: function (data) {
                self.info('');
                if (data.text) {
                    self.success(data.text);
                } else {
                    self.success("Service transaction was successful. Check console for output.");
                    console.log(data);
                }
            }//,
            //error: function (data) {
            //    debugger;
            //    console.log(data);
            //}
        }).fail(function (jqXHR, textStatus, errorThrown) {
            self.info('');
            self.error(errorThrown + "<p>" + JSON.stringify(jqXHR.responseText, null, 2));
        });
    }


};

ko.applyBindings(new ViewModel());