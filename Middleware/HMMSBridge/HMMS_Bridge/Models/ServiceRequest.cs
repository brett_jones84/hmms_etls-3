﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HMMS_Bridge.Models
{
    public class ServiceRequest
    {
        [Required]
        [Key]
        public string CSC_SR_ID { get; set; }
        public string HMMS_SR_ID { get; set; }
        public string Status { get; set; }
        public string WorkArea_ID { get; set; }

    }
}