﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HMMS_Bridge.DTOs
{
    public class CommentDto
    {
        public int ID { get; set; }
        public string VDOT_SRID { get; set; }
        public string VDOT_WOID{ get; set; }
        public string CommentText { get; set; }
        public string CommentType { get; set; }
        public DateTime CommentCreationDate { get; set; }
        public DateTime CommentUpdateDate { get; set; }
        //public DateTime RequestTimeStamp { get; set; }
        //public DateTime TimeStampReceived { get; set; }
        //public DateTime TimeStampSent { get; set; }
    }
}