The following steps will create the necessary objects to create a view that monitors the last time the ETL stored procedures were run.  
The databases that this script will affect are set in the main.bat file with the following parameters and default values:

MAIN_DB="HMMS" 
STAGING_DB="HMMS_STAGING"
MSCRM="CSC_MSCRM"

If using different databases please change the values accordingly. For the purposes of this document the databases will be referred to by their variable name.

Step 1: Take a backup of the MAIN_DB database.

Step 2: Ensure person executing scripts has the following permissions in the target server databases...

	a) create a table in the STAGING_DB database
	b) edit table data in MAIN_DB and STAGING_DB
	c) create procedure in MAIN_DB
	d) create and assign owner_login_name for job ( https://msdn.microsoft.com/en-us/library/ms187901.aspx ).  The job must also be configured to run as a login that has permissions to execute SQL Agent jobs.
	e) read data from MSCRM database (for views)

Step 3: Ensure that all files are in the same directory:

	vdot_vw_hmms_ETL_Execution_Time.sql	
	main.bat
	main.sql


Step 4: Run main.bat (this executes main.sql which calls most of the necessasary steps).  the output from this is appended to results.log

ROLLBACK:
	Restore the MAIN_DB database back to the backup point taken at the beginning of these instructions.