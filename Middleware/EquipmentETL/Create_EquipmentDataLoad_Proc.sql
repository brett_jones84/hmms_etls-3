USE $(MAIN_DB)
GO

-- PRINT '$(MAIN_DB)'
-- PRINT '$(STAGING_DB)'
-- PRINT '$(MSCRM)'

/****** 
--		Object:  	StoredProcedure [dbo].[EquipmentLoad]    
--		Author: 	Brett Jones (WorldView Solutions)
--		History: 	Version 1.0,	10/4/2017, 	Brett Jones
--					Initial creation of ETL process for pulling M5 Equipment data from the HMMS_STAGING database.
******/


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('EquipmentDataLoad') IS NOT NULL
	BEGIN
		PRINT N'Dropping existing EquipmentDataLoad Procedure...';  
		DROP PROCEDURE  [dbo].[EquipmentDataLoad];
	END
GO

--PRINT N'Creating LaborDataLoad Procedure...';  
CREATE procedure [dbo].[EquipmentDataLoad] 
	@cleanLoad INT = 0 --if 1, wipe any existing data 
as
begin

PRINT N'EquipmentDataLoad Procedure Beginning...';  
BEGIN TRANSACTION [Tran1]

BEGIN TRY

	IF @cleanLoad = 1
		BEGIN

			PRINT N'cleanLoad param is set.  Purging existing data...';
			

			
			PRINT N'Truncating tbl_ResMgr_Equipment...';  
			TRUNCATE TABLE [dbo].[tbl_ResMgr_Equipment];

			PRINT N'Truncating tbl_ResMgr_Equipment_ExData...';  
			TRUNCATE TABLE [dbo].[tbl_ResMgr_Equipment_ExData]

			PRINT N'Truncating tbl_ResMgr_Equipment_Rates...';  
			TRUNCATE TABLE [dbo].[tbl_ResMgr_Equipment_Rates]

			
		END

	-----------------------------------------------------------------
	-----------------  tbl_ResMgr_Equipment_Categories  -----------------
	-----------------------------------------------------------------

	PRINT N'Truncating tbl_ResMgr_Equipment_Categories...';  			
	TRUNCATE TABLE dbo.[tbl_ResMgr_Equipment_Categories];	
	
	INSERT INTO tbl_ResMgr_Equipment_Categories (Name, Description)
	
	SELECT * FROM (SELECT 
		CATEGORY_DESC AS Name
		,CATEGORY_DESC AS Description   
	FROM [$(STAGING_DB)].[dbo].[M5_HMMS_EQUIPMENTS]
	GROUP BY CATEGORY_DESC ) AS Source

	WHERE NOT EXISTS (SELECT Name FROM tbl_ResMgr_Equipment_Categories Target WHERE Target.Name = Source.Name )
		
	-----------------------------------------------------------------
	-----------------  tbl_ResMgr_Equipment  ------------------------
	-----------------------------------------------------------------
		PRINT N'Processing tbl_ResMgr_Equipment...';  

	--set identity_insert dbo.tbl_ResMgr_Equipment ON;

	merge tbl_ResMgr_Equipment as Target
	using (
		SELECT	
			  s.UNIT_NO
			, s.CATEGORY_DESC
			, s.CATEGORY
			, s.CARDINAL_DEPT_ID
			, c.Category_ID
			, c.Name
			, d.DEPARTMENTID
			, d.DESCRIPTION
			, d.DISTRICTNAME
			, pg.Perm_ID
			, pg.Name AS Permission_Name
		FROM [$(STAGING_DB)].[dbo].[M5_HMMS_EQUIPMENTS] s
		LEFT JOIN [dbo].[tbl_ResMgr_Equipment_Categories] c
			on c.Name = s.CATEGORY
		LEFT JOIN $(STAGING_DB).dbo.DEB_CARDINAL_DEPARTMENTTREE d
			on d.DEPARTMENTID = s.CARDINAL_DEPT_ID
		LEFT JOIN tbl_ResMgr_Permission_Groups pg
			on pg.name = d.DISTRICTNAME				
	) AS Source

	on Target.Number = Source.UNIT_NO
	when matched then
		update set 
				Target.Name = source.CATEGORY_DESC
			--, Target.Equipment_ID = source.UNIT_NO
			, Target.External_ID = source.UNIT_NO
			, Target.department_id = -1
			, Target.Type_ID = 1
			, Target.Available = 1
			, Target.newrecord = 0
			, Target. perm_ID = source.perm_ID
			, Target.updatedDate = CURRENT_TIMESTAMP
			, Target.Category_ID = source.Name	
			, Target.Mfr_ID = -1
			, Target.Splr_ID = -1	
			, Target.Program_ID = -1
			, Target.LoggedBy_ID = 203 --administrator account	
			, Target.UpdatedBy_ID = 203 --administrator account		
			, Target.LoggedDate = CURRENT_TIMESTAMP	
			, Target.Standard = 0.00
			, IsDeleted = 0
	when not matched by target then
		insert (
			  Name
			, External_ID
			, Number
			, department_id
			, Type_ID
			, Available
			, newrecord
			, perm_ID
			, updatedDate
			, Category_ID
			, Mfr_ID 
			, Splr_ID 
			, Program_ID
			, LoggedBy_ID
			, UpdatedBy_ID
			, LoggedDate
			, Standard
			, IsDeleted
		) values (
			  source.CATEGORY_DESC
			, source.UNIT_NO
			, source.UNIT_NO
			, -1
			, 1
			, 1
			, 0
			, source.perm_ID
			, CURRENT_TIMESTAMP
			, source.Name
			, -1
			, -1
			, -1
			, 203 -- administrator account
			, 203 -- administrator account
			, CURRENT_TIMESTAMP
			, 0.00
			, 0
		)
	when not matched by source
		then delete;

	--set identity_insert dbo.tbl_ResMgr_Equipment OFF;




	-----------------------------------------------------------------
	-----------------  tbl_ResMgr_Equipment_ExData  -----------------
	-----------------------------------------------------------------
	PRINT N'Processing tbl_ResMgr_Equipment_ExData...';  

	merge tbl_ResMgr_Equipment_ExData as Target
	using (
			SELECT 
				e.Equipment_ID
				, s.UNIT_NO
				, s.CATEGORY_DESC
				, s.CATEGORY
				, s.M5_DEPT_NO
				, s.M5_DEPT_DESCRIPTION
				, s.CARDINAL_DEPT_ID
				, s.LEASE_RATE
				, s.LEASE_TERM
				, d.DEPARTMENTID
				, d.DESCRIPTION
			FROM 
			$(MAIN_DB).dbo.tbl_ResMgr_Equipment as e
			LEFT JOIN 
				$(STAGING_DB).DBO.M5_HMMS_EQUIPMENTS as s ON e.Number = s.UNIT_NO
			LEFT JOIN 
				$(STAGING_DB).DBO.DEB_CARDINAL_DEPARTMENTTREE d  on d.DEPARTMENTID = s.CARDINAL_DEPT_ID
		) as Source
	on (target.Equipment_ID = source.Equipment_ID)
	when matched then
		update set		
			Target.Text1 = source.M5_DEPT_NO
			, Target.Text3 = source.M5_DEPT_DESCRIPTION
			, Target.Text5 = source.CARDINAL_DEPT_ID
			, Target.Text6 = source.DESCRIPTION
			, Target.Text7 = source.CATEGORY
	when not matched by target then
		insert (
			Equipment_ID
			, Text1
			, Text3
			, Text5
			, Text6
			, Text7)
		values (
			source.Equipment_ID
			, source.M5_DEPT_NO
			, source.M5_DEPT_DESCRIPTION
			, source.CARDINAL_DEPT_ID
			, source.DESCRIPTION, 
			, source.CATEGORY)
	when not matched by source then
		delete;
	

	
	
	
	
	
	
	-----------------------------------------------------------------
	-----------------  tbl_ResMgr_Equipment_Rates  -----------------
	-----------------------------------------------------------------
	PRINT N'Processing tbl_ResMgr_Equipment_Rates...';  

	merge tbl_ResMgr_Equipment_Rates as Target
	using (
			SELECT 
				e.Equipment_ID
				, r.UNIT_NO				
				, r.Rate_ID
				, r.LEASE_TERM
				, r.LEASE_RATE

			FROM 
			$(MAIN_DB).dbo.tbl_ResMgr_Equipment as e
			LEFT JOIN 
				$(MAIN_DB).DBO.vdot_equipment_rates as r ON e.External_ID = r.UNIT_NO			
		) as Source
	on (target.Equipment_ID = source.Equipment_ID AND target.Rate_ID = source.Rate_ID)
	when matched then
		update set		
			Target.Rate = source.LEASE_RATE			
	when not matched by target then
		insert (
			Equipment_ID
			, Rate_ID
			, Rate)
		values (
			source.Equipment_ID
			, source.Rate_ID
			, source.LEASE_RATE)
	when not matched by source then
		delete;




-----------------------------------------------------------------
		-----------------  tbl_Maintenance_Activity_UserListItems  -----------------
		-----------------------------------------------------------------
		---------------------------------------------------------
		--Equipment ID / Name
		--ID 62		
		PRINT N'Processing tbl_Maintenance_Activity_UserListItems Equipment ID / Name Items...'; 
		---------------------------------------------------------
		MERGE dbo.tbl_Maintenance_Activity_UserListItems AS target 
		using (
			SELECT UNIT_NO +' '+ CATEGORY_DESC as item
			FROM [$(STAGING_DB)].[dbo].[M5_HMMS_EQUIPMENTS]			
			) AS source 
		ON ( target.item = source.item and target.list_id=62) 
		WHEN matched AND target.item <> source.item THEN 
			UPDATE SET target.item = source.item 
		WHEN NOT matched BY target THEN 
			INSERT (list_id, item) 
		VALUES ( 62,
			source.item)
		WHEN NOT MATCHED BY SOURCE THEN
			DELETE;
		
		select * from dbo.tbl_Maintenance_Activity_UserListItems where list_id = 62;







	PRINT N'Committing transaction...'; 
	COMMIT TRANSACTION [Tran1];

END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION [Tran1];

	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT	@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

	--TODO: email?

	-- return error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
END CATCH  

PRINT N'...EquipmentDataLoad Complete'; 

END
 
GO


