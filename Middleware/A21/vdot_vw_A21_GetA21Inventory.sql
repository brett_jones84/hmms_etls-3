USE [$(MAIN_DB)]
GO

IF OBJECT_ID('GetA21Inventory') IS NOT NULL
	DROP FUNCTION  [dbo].[GetA21Inventory]
GO

/****** Object:  UserDefinedFunction [dbo].[GetA21Inventory]    Script Date: 1/18/2018 10:19:29 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Michael Kolonay
-- Create date: 12/15/2017
-- Description:	Function to get Inventory used in WO's by department
-- =============================================
CREATE FUNCTION [dbo].[GetA21Inventory] 
(
	-- Add the parameters for the function here
	@departmentID int, 
	@date date
)
RETURNS 
@A21Inventory TABLE 
(
	-- Add the column definitions for the TABLE variable here
		[ID] int
		,[Work_Order_Id] varchar(100)
		,[Is_Disaster] varchar(3)
		,[Speedtype] varchar(8)
		,[Date_Of_Work] date
		,[Department_ID] varchar(5)
		,[Dept_Description] varchar(150)
		,[Cost_Center_Code] varchar(8)
		,[Project] varchar(50)
		,[Task_Code] varchar(5)
		,[Task_Description] varchar(50)
		,[Activity_Code] varchar(3)
		,[Activity_Description] varchar(50)
		,[FIPS] varchar(50)
		,[Agency1__AU] varchar(150)
		,[Agency2__AU2] varchar(50)
		,[Asset] varchar(50)
		,[FormType] varchar(7)
		,[Charge To Department_ID] varchar(5)
		,[Warehouse_Department_ID] varchar(512)
		,[Detail_Department_ID] varchar(512)
		,[Description] varchar(100)
		,[TransactionType] varchar(256)
		,[Quantity] decimal(18,4)
		,[Quantity Issued] decimal(18,4)
		,[Quantity Returned] decimal(18,4)
		,[UOM] varchar(50)
		,[IMS Stock Location] int
		,[NIGP Code or IMS Stock No] varchar(50)
		,[Cond Code] varchar(50)
)
AS
BEGIN
		INSERT INTO @A21Inventory SELECT 
			  wo.[ID]
			, wo.[Work_Order_Id]
			, wo.[Is_Disaster]
			, wo.[Speedtype]
			, wo.[Date_Of_Work]
			, wo.[Department_ID]
			, wo.[Dept_Description]
			, wo.[Cost_Center_Code]
			, wo.[Project]
			, wo.[Task_Code]
			, wo.[Task_Description]
			, wo.[Activity_Code]
			, wo.[Activity_Description]
			, wo.[FIPS]
			, wo.[Agency1__AU]
			, wo.[Agency2__AU2]
			, wo.[Asset]
			, wo.[FormType]
			, wo.[Charge To Department_ID]
			, wh.Description AS Warehouse_Department_ID
			, COALESCE(NULLIF(wo.[Charge To Department_ID],''),wo.[Department_ID]) AS Detail_Department_ID	
			, it.Name AS [Description]
			, it.TransactionType
			, it.Quantity
			, CASE 
				WHEN 
					it.TransactionType IN ('Issue','Transfer Out','Reserve')
				THEN 
					it.Quantity
				ELSE
					0
				END AS [Quantity Issued]
			, CASE 
				WHEN 
					it.TransactionType IN ('Receive','Return','Transfer In')
				THEN	
					it.Quantity
				ELSE 
					0
				END AS [Quantity Returned]
			, i.Units AS UOM
			, it.WarehouseID AS [IMS Stock Location]
			, i.ManufacturerNumber AS [NIGP Code or IMS Stock No]
			, i.SupplierNumber as [Cond Code]		

	from 
		reports.InventoryTransactions AS it 
		LEFT JOIN reports.Inventory AS i ON it.ID = i.ID
		LEFT JOIN dbo.tbl_ResMgr_Warehouse AS wh ON it.WarehouseID = wh.Warehouse_ID 
		LEFT JOIN [dbo].[vdot_vw_A21_base] AS wo ON IT.WorkOrderID =  wo.ID
		where wh.Description = @departmentID and wo.[Date_Of_Work]= @date
	--where wh.Description = @departmentID and wo.[Date_Of_Work]= @date
	--where wh.Description = 11041 and wo.[Date_Of_Work]= '2017-11-28'
	
	RETURN 
END

GO


