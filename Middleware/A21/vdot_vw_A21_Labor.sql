USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_vw_A21_Labor') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_A21_Labor]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [dbo].[vdot_vw_A21_Labor] AS
SELECT 
	  wo.[ID]
    , wo.[Work_Order_Id]
    , wo.[Is_Disaster]
    , wo.[Speedtype]
    , wo.[Date_Of_Work]
    , wo.[Department_ID]
    , wo.[Dept_Description]
    , wo.[Cost_Center_Code]
    , wo.[Project]
    , wo.[Task_Code]
    , wo.[Task_Description]
    , wo.[Activity_Code]
    , wo.[Activity_Description]
    , wo.[FIPS]
    , wo.[Agency1__AU]
    , wo.[Agency2__AU2]
    , wo.[Asset]
    , wo.[FormType]
	, wo.[Charge To Department_ID]
	, COALESCE(NULLIF(wo.[Charge To Department_ID],''),ru_p.DepartmentId2) AS Detail_Department_ID
	, ru.EmployeeName AS Employee_Full_Name
	, ru_p.FirstName
	, ru_p.LastName	
	, ru_p.DepartmentId2 AS Employee_Department_ID	
	, ru.RateName AS Time_Recording_Charge 
	, ru.EmployeeID AS Employee_ID
	, ru.Hours AS Resource_Quantity
	, 'hours' AS Resource_UOM	
	, ru.EquipmentID
	, 'NEED THIS' AS EquipmentHours
	

FROM 
	[dbo].[vdot_vw_A21_base] AS wo 		
	LEFT JOIN reports.ResourceUsage AS ru ON wo.ID = ru.Record_ID
	LEFT JOIN [HMMS].[reports].[Personnel] AS ru_p ON ru.EmployeeID = ru_p.EmployeeNumber
WHERE wo.[Department_ID] IS NOT NULL AND ru.EmployeeID != -1




GO


