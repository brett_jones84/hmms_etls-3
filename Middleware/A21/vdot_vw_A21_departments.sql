USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_vw_A21_departments') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_A21_departments]
GO

/****** Object:  View [dbo].[vdot_vw_A21_departments]    Script Date: 9/18/2017 2:42:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vdot_vw_A21_departments]
AS
SELECT   DepartmentID AS Department_ID, DepartmentName AS Dept_Description
FROM     reports.WorkOrders AS wo
GROUP BY DepartmentID, DepartmentName

GO