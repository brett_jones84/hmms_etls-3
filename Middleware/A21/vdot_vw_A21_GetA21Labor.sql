USE [$(MAIN_DB)]
GO

IF OBJECT_ID('GetA21Labor') IS NOT NULL
	DROP FUNCTION  [dbo].[GetA21Labor]
GO

/****** Object:  UserDefinedFunction [dbo].[GetA21Labor]    Script Date: 1/18/2018 10:21:07 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Michael Kolonay
-- Create date: 12/14/2017
-- Description:	Gets all the labor for a department including those that have not recorded hours
-- =============================================
CREATE FUNCTION [dbo].[GetA21Labor] 
(
	-- Add the parameters for the function here
	@departmentID int, 
	@date date
)
RETURNS 
@A21_Labor TABLE 
(
	-- Add the column definitions for the TABLE variable here
	[Detail_Department_ID] varchar(5)
	,[Employee_Full_Name] varchar(100)
	,[FirstName] varchar(50)
	,[LastName] varchar(50)
	,[Employee_Department_ID] varchar(50)
	,[Time_Recording_Charge] varchar(50)
	,[Employee_ID] varchar(50)
	,[Resource_Quantity] decimal(18,4)
	,[Resource_UOM] varchar(5)
	,[ID] int
	,[Work_Order_Id] varchar(100)
	,[Is_Disaster] varchar(3)
	,[Speedtype] varchar(8)
	,[Date_Of_Work] date
	,[Department_ID] varchar(5)
	,[Dept_Description] varchar(150)
	,[Cost_Center_Code] varchar(8)
	,[Project] varchar(50)
	,[Task_Code] varchar(5)
	,[Task_Description] varchar(50)
	,[Activity_Code] varchar(3)
	,[Activity_Description] varchar(50)
	,[FIPS] varchar(50)
	,[Agency1__AU] varchar(150)
	,[Agency2__AU2] varchar(50)
	,[Asset] varchar(50)
	,[FormType] varchar(7)
	,[Charge To Department_ID] varchar(5)
)
AS
BEGIN
	DECLARE @WORK TABLE(
		[Detail_Department_ID] varchar(5)
		,[Employee_Full_Name] varchar(100)
		,[FirstName] varchar(50)
		,[LastName] varchar(50)
		,[Employee_Department_ID] varchar(50)
		,[Time_Recording_Charge] varchar(50)
		,[Employee_ID] varchar(50)
		,[Resource_Quantity] decimal(18,4)
		,[Resource_UOM] varchar(5)
		,[ID] int
		,[Work_Order_Id] varchar(100)
		,[Is_Disaster] varchar(3)
		,[Speedtype] varchar(8)
		,[Date_Of_Work] date
		,[Department_ID] varchar(5)
		,[Dept_Description] varchar(150)
		,[Cost_Center_Code] varchar(8)
		,[Project] varchar(50)
		,[Task_Code] varchar(5)
		,[Task_Description] varchar(50)
		,[Activity_Code] varchar(3)
		,[Activity_Description] varchar(50)
		,[FIPS] varchar(50)
		,[Agency1__AU] varchar(150)
		,[Agency2__AU2] varchar(50)
		,[Asset] varchar(50)
		,[FormType] varchar(7)
		,[Charge To Department_ID] varchar(5)
);


WITH src AS (
SELECT 
	 COALESCE(NULLIF(wo.[Charge To Department_ID],''),wo.[Department_ID]) AS Detail_Department_ID
	, p.FullName AS Employee_Full_Name
	, p.FirstName
	, p.LastName	
	, ru_p.DepartmentId2 AS Employee_Department_ID	
	, l.RateName AS Time_Recording_Charge 
	, p.EmployeeNumber AS Employee_ID
	, l.Hours AS Resource_Quantity
	, 'hours' AS Resource_UOM			
	,  wo.[ID]
    , wo.[Work_Order_Id]
    , wo.[Is_Disaster]
    , wo.[Speedtype]
    , wo.[Date_Of_Work]
    , wo.[Department_ID]
    , wo.[Dept_Description]
    , wo.[Cost_Center_Code]
    , wo.[Project]
    , wo.[Task_Code]
    , wo.[Task_Description]
    , wo.[Activity_Code]
    , wo.[Activity_Description]
    , wo.[FIPS]
    , wo.[Agency1__AU]
    , wo.[Agency2__AU2]
    , wo.[Asset]
    , wo.[FormType]
	, wo.[Charge To Department_ID]


	FROM [$(MAIN_DB)].[reports].[Personnel] AS p
		LEFT JOIN [$(MAIN_DB)].[reports].[WorkOrderActualLabor] AS l ON p.EmployeeNumber = l.EmployeeID
		LEFT JOIN DBO.vdot_vw_A21_base AS wo on l.ID = wo.ID
		LEFT JOIN [$(MAIN_DB)].[reports].[Personnel] AS ru_p ON p.EmployeeNumber = ru_p.EmployeeNumber	
	WHERE  ru_p.DepartmentId2 = @departmentID  AND Date_Of_Work = @date


)

	INSERT INTO @WORK 
	SELECT * FROM src;
	
	INSERT INTO @A21_Labor
	SELECT 
		w.[Detail_Department_ID]
		,COALESCE(w.[Employee_Full_Name],p.FullName) AS Employee_Full_Name
		,COALESCE(w.[FirstName], p.FirstName) AS FirstName
		,COALESCE(w.[LastName], p.LastName) AS LastName
		,COALESCE(w.[Employee_Department_ID], p.DepartmentId2) AS Employee_Department_ID
		,w.[Time_Recording_Charge]
		,COALESCE(w.[Employee_ID], p.EmployeeNumber) AS Employee_ID
		,w.[Resource_Quantity]
		,w.[Resource_UOM]
		,w.[ID]
		,w.[Work_Order_Id]
		,w.[Is_Disaster]
		,w.[Speedtype]
		,w.[Date_Of_Work]
		,w.[Department_ID]
		,w.[Dept_Description]
		,w.[Cost_Center_Code]
		,w.[Project]
		,w.[Task_Code]
		,w.[Task_Description]
		,w.[Activity_Code]
		,w.[Activity_Description]
		,w.[FIPS]
		,w.[Agency1__AU] 
		,w.[Agency2__AU2]
		,w.[Asset]
		,w.[FormType]
		,w.[Charge To Department_ID]
	FROM @WORK as w
		FULL OUTER JOIN (SELECT * FROM [$(MAIN_DB)].[reports].[Personnel]
	WHERE EmployeeNumber NOT IN (SELECT DISTINCT Employee_ID from @WORK) AND DepartmentId2 = @departmentID) AS p 
	ON w.Employee_ID = p.EmployeeNumber
	
	RETURN 
END

GO


