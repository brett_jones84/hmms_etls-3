USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_vw_A21_ChartFieldNumbers') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_A21_ChartFieldNumbers]
GO

/****** Object:  View [dbo].[vdot_vw_A21_ChartFieldNumbers]    Script Date: 9/18/2017 2:41:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vdot_vw_A21_ChartFieldNumbers] AS

WITH cfsrc as (
	Select 
	Speedtype
	,Cost_Center_Code
	,Department_ID
	,Dept_Description
	,Task_Code
	,FIPS
	,Asset
	,Agency1__AU
	,Agency2__AU2
	,Project
	,Activity_Description
	,Date_Of_Work
	,ROW_NUMBER() OVER (PARTITION BY Date_Of_Work,Department_ID ORDER BY Date_Of_Work ASC) as Group_Number 
from (
		select 
		Speedtype
		,Cost_Center_Code
		,Department_ID
		,Dept_Description
		,Task_Code
		,FIPS
		,Asset
		,Agency1__AU
		,Agency2__AU2
		,Project
		,Activity_Description
		,Date_Of_Work 
		from [dbo].[vdot_vw_A21]
		WHERE Resource_Type in ('LABOR','MATERIAL') 
		GROUP BY Speedtype,Cost_Center_Code,Department_ID,Dept_Description,Task_Code,FIPS,Asset,Agency1__AU,Agency2__AU2,Project,Activity_Description,Date_Of_Work
) t)


select  

[ID]
      ,src.[Work_Order_Id]
      ,src.[Is_Disaster]
      ,src.[Speedtype]
      ,src.[Date_Of_Work]
      ,src.[Department_ID]
      ,src.[Dept_Description]
      ,src.[Cost_Center_Code]
      ,src.[Project]
      ,src.[Task_Code]
      ,src.[Task_Description]
      ,src.[Activity_Code]
      ,src.[Activity_Description]
      ,src.[FIPS]
      ,src.[Agency1__AU]
      ,src.[Agency2__AU2]
      ,src.[Asset]
      ,src.[Resource_Type]
      ,src.[Resource]
      ,src.[Time_Recording_Charge]
      ,src.[Employee_ID]
      ,src.[Resource_Quantity]
      ,src.[Resource_UOM]
      ,src.[Stock_Location]
      ,src.[NIGP_CODE]
      ,src.[Condition]
	  , cfsrc.Group_Number
from [dbo].[vdot_vw_A21] as src
join cfsrc on 
	src.Speedtype = cfsrc.Speedtype
	AND src.Cost_Center_Code = cfsrc.Cost_Center_Code
	AND src.Department_ID = cfsrc.Department_ID
	AND src.Dept_Description = cfsrc.Dept_Description
	AND src.Task_Code = cfsrc.Task_Code
	AND src.FIPS = cfsrc.FIPS
	AND src.Asset = cfsrc.Asset
	AND src.Agency1__AU = cfsrc.Agency1__AU
	AND src.Agency2__AU2 = cfsrc.Agency2__AU2
	AND src.Project = cfsrc.Project
	AND src.Activity_Description = cfsrc.Activity_Description
	AND src.Date_Of_Work = cfsrc.Date_Of_Work 

GO