USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_vw_A21_Inventory') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_A21_Inventory]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO










CREATE VIEW [dbo].[vdot_vw_A21_Inventory] AS
SELECT 
		  wo.[ID]
		, wo.[Work_Order_Id]
		, wo.[Is_Disaster]
		, wo.[Speedtype]
		, wo.[Date_Of_Work]
		, wo.[Department_ID]
		, wo.[Dept_Description]
		, wo.[Cost_Center_Code]
		, wo.[Project]
		, wo.[Task_Code]
		, wo.[Task_Description]
		, wo.[Activity_Code]
		, wo.[Activity_Description]
		, wo.[FIPS]
		, wo.[Agency1__AU]
		, wo.[Agency2__AU2]
		, wo.[Asset]
		, wo.[FormType]
		, wo.[Charge To Department_ID]
		, wh.Description AS Warehouse_Department_ID
		, COALESCE(NULLIF(wo.[Charge To Department_ID],''),wh.Description ) AS Detail_Department_ID		
		, it.Name AS [Description]
		, it.TransactionType
		, it.Quantity
		, CASE 
			WHEN 
				it.TransactionType IN ('Issue','Transfer Out','Reserve')
			THEN 
				it.Quantity
			ELSE
				0
			END AS [Quantity Issued]
		, CASE 
			WHEN 
				it.TransactionType IN ('Receive','Return','Transfer In')
			THEN	
				it.Quantity
			ELSE 
				0
			END AS [Quantity Returned]
		, i.Units AS UOM
		, it.WarehouseID AS [IMS Stock Location]
		, i.ManufacturerNumber AS [NIGP Code or IMS Stock No]
		, i.SupplierNumber as [Cond Code]

FROM 
	[dbo].[vdot_vw_A21_base] AS wo 		
	LEFT JOIN [dbo].[vdot_vw_hmms_WorkOrder_Linear_Default] AS wodf ON wo.ID = wodf.ID
	LEFT JOIN reports.InventoryTransactions AS it ON wo.ID = it.WorkOrderID
	LEFT JOIN reports.Inventory AS i ON it.ID = i.ID
	LEFT JOIN dbo.tbl_ResMgr_Warehouse AS wh ON it.WarehouseID = wh.Warehouse_ID 


where wodf.[Department ID] is not null and it.Name is not null








GO
