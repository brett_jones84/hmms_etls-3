USE [$(MAIN_DB)]
GO

IF OBJECT_ID('v_labor_rates') IS NOT NULL
	DROP VIEW  [dbo].[v_labor_rates]
GO

create view v_labor_rates as
		--convert null fringe amounts to 0
		with clean_src as
		(
			SELECT	s.EMP_ID Employee_ID
					, s.[EMP_TYPE_CD]
					, s.[HOURLY_RATE_NBR]
					, CASE WHEN s.[MPFPT_Fringe] IS NULL THEN 0
						ELSE s.[MPFPT_Fringe]
					  END MPFPT_Fringe
			FROM [$(STAGING_DB)].[dbo].[EMPLOYEE_JOB_DATA] s
		)
		--RATE 1 (RGH)
		SELECT	s.Employee_ID
				, 1 Rate_ID
				, 'RGH' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, CASE WHEN s.[EMP_TYPE_CD] = 'S' THEN 0 
						ELSE s.[HOURLY_RATE_NBR] + (s.[MPFPT_Fringe]/2080)
					END val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 2 (RGS)
		SELECT	s.Employee_ID
				, 2 Rate_ID
				, 'RGS' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, CASE	WHEN s.[EMP_TYPE_CD] = 'H' THEN 0 
						ELSE s.[HOURLY_RATE_NBR] + (s.[MPFPT_Fringe]/2080)
					END val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 3 (COH)
		SELECT	s.Employee_ID
				, 3 Rate_ID
				, 'COH' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, CASE	WHEN s.[EMP_TYPE_CD] = 'S' THEN 0 
						ELSE s.[HOURLY_RATE_NBR] * 1.5
					END val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 4 (CPE)
		SELECT	s.Employee_ID
				, 4 Rate_ID
				, 'CPE' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, s.[HOURLY_RATE_NBR] val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 5 (CPT)
		SELECT	s.Employee_ID
				, 5 Rate_ID
				, 'CPT' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, s.[HOURLY_RATE_NBR] val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 6 (ECP)
		SELECT	s.Employee_ID
				, 6 Rate_ID
				, 'ECP' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, s.[HOURLY_RATE_NBR] val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 7 (EO2)
		SELECT	s.Employee_ID
				, 7 Rate_ID
				, 'EO2' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, CASE	WHEN s.[EMP_TYPE_CD] = 'S' THEN 0 
						ELSE s.[HOURLY_RATE_NBR] * 1.5
					END val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 8 (ES1)
		SELECT	s.Employee_ID
				, 8 Rate_ID
				, 'ES1' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, CASE	WHEN s.[EMP_TYPE_CD] = 'H' THEN 0 
						ELSE s.[HOURLY_RATE_NBR]
					END val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 9 (OS1)
		SELECT	s.Employee_ID
				, 9 Rate_ID
				, 'OS1' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, s.[HOURLY_RATE_NBR] val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 10 (OT2)
		SELECT	s.Employee_ID
				, 10 Rate_ID
				, 'OT2' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, CASE	WHEN s.[EMP_TYPE_CD] = 'S' THEN 0 
						ELSE s.[HOURLY_RATE_NBR] * 1.5
					END val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 11 (UOT)
		SELECT	s.Employee_ID
				, 11 Rate_ID
				, 'UOT' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 12 (COE)
		SELECT	s.Employee_ID
				, 12 Rate_ID
				, 'COE' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, CASE	WHEN s.[EMP_TYPE_CD] = 'H' THEN 0 
						ELSE s.[HOURLY_RATE_NBR]
					END val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 13 (COP)
		SELECT	s.Employee_ID
				, 13 Rate_ID
				, 'COP' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, CASE	WHEN s.[EMP_TYPE_CD] = 'H' THEN 0 
						ELSE s.[HOURLY_RATE_NBR] * 1.5
					END val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 14 (EO1)
		SELECT	s.Employee_ID
				, 14 Rate_ID
				, 'EO1' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, CASE	WHEN s.[EMP_TYPE_CD] = 'H' THEN 0 
						ELSE s.[HOURLY_RATE_NBR] * 1.5
					END val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 15 (OT1)
		SELECT	s.Employee_ID
				, 15 Rate_ID
				, 'OT1' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, CASE	WHEN s.[EMP_TYPE_CD] = 'H' THEN 0 
						ELSE s.[HOURLY_RATE_NBR] * 1.5
					END val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 17 (ERG)
		SELECT	s.Employee_ID
				, 17 Rate_ID
				, 'ERG' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, CASE	WHEN s.[EMP_TYPE_CD] = 'S' THEN 0 
						ELSE s.[HOURLY_RATE_NBR] + (s.[MPFPT_Fringe]/2080)
					END val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 18 (CLO)
		SELECT	s.Employee_ID
				, 18 Rate_ID
				, 'CLO' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 19 (HFL)
		SELECT	s.Employee_ID
				, 19 Rate_ID
				, 'HFL' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 20 (NHO)
		SELECT	s.Employee_ID
				, 20 Rate_ID
				, 'NHO' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 21 (ADM)
		SELECT	s.Employee_ID
				, 21 Rate_ID
				, 'ADM' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 22 (ALI)
		SELECT	s.Employee_ID
				, 22 Rate_ID
				, 'ALI' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 23 (BMO)
		SELECT	s.Employee_ID
				, 23 Rate_ID
				, 'BMO' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 24 (CPO)
		SELECT	s.Employee_ID
				, 24 Rate_ID
				, 'CPO' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 25 (CSL)
		SELECT	s.Employee_ID
				, 25 Rate_ID
				, 'CSL' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 26 (DLR)
		SELECT	s.Employee_ID
				, 26 Rate_ID
				, 'DLR' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 27 (SK)
		SELECT	s.Employee_ID
				, 27 Rate_ID
				, 'SK' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 28 (DSR)
		SELECT	s.Employee_ID
				, 28 Rate_ID
				, 'DSR' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 29 (ELP)
		SELECT	s.Employee_ID
				, 29 Rate_ID
				, 'ELP' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 30 (ELW)
		SELECT	s.Employee_ID
				, 30 Rate_ID
				, 'ELW' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 31 (EMS)
		SELECT	s.Employee_ID
				, 31 Rate_ID
				, 'EMS' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 32 (EOL)
		SELECT	s.Employee_ID
				, 32 Rate_ID
				, 'EOL' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 33 (EPT)
		SELECT	s.Employee_ID
				, 33 Rate_ID
				, 'EPT' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 34 (ERG)
		SELECT	s.Employee_ID
				, 34 Rate_ID
				, 'ERG' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 35 (ERL)
		SELECT	s.Employee_ID
				, 35 Rate_ID
				, 'ERL' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 36 (ESP)
		SELECT	s.Employee_ID
				, 36 Rate_ID
				, 'ESP' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 37 (FML)
		SELECT	s.Employee_ID
				, 37 Rate_ID
				, 'FML' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 38 (HOL)
		SELECT	s.Employee_ID
				, 38 Rate_ID
				, 'HOL' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 39 (LNP)
		SELECT	s.Employee_ID
				, 39 Rate_ID
				, 'LNP' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 40 (LTD)
		SELECT	s.Employee_ID
				, 40 Rate_ID
				, 'LTD' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 41 (MBL)
		SELECT	s.Employee_ID
				, 41 Rate_ID
				, 'MBL' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 42 (MIL)
		SELECT	s.Employee_ID
				, 42 Rate_ID
				, 'MIL' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 43 (MIP)
		SELECT	s.Employee_ID
				, 43 Rate_ID
				, 'MIP' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 44 (MLD)
		SELECT	s.Employee_ID
				, 44 Rate_ID
				, 'MLD' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 45 (OCP)
		SELECT	s.Employee_ID
				, 45 Rate_ID
				, 'OCP' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 46 (OPO)
		SELECT	s.Employee_ID
				, 46 Rate_ID
				, 'OPO' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 47 (PTL)
		SELECT	s.Employee_ID
				, 47 Rate_ID
				, 'PTL' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 48 (OTT)
		SELECT	s.Employee_ID
				, 48 Rate_ID
				, 'OTT' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 49 (PD1)
		SELECT	s.Employee_ID
				, 49 Rate_ID
				, 'PD1' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 50 (PD2)
		SELECT	s.Employee_ID
				, 50 Rate_ID
				, 'PD2' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 51 (PER)
		SELECT	s.Employee_ID
				, 51 Rate_ID
				, 'PER' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 52 (PHE)
		SELECT	s.Employee_ID
				, 52 Rate_ID
				, 'PHE' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 53 (PLL)
		SELECT	s.Employee_ID
				, 53 Rate_ID
				, 'PLL' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 54 (SCK)
		SELECT	s.Employee_ID
				, 54 Rate_ID
				, 'SCK' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 55 (SDP)
		SELECT	s.Employee_ID
				, 55 Rate_ID
				, 'SDP' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 56 (STD)
		SELECT	s.Employee_ID
				, 56 Rate_ID
				, 'STD' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 57 (STL)
		SELECT	s.Employee_ID
				, 57 Rate_ID
				, 'STL' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 58 (VAC)
		SELECT	s.Employee_ID
				, 58 Rate_ID
				, 'VAC' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 59 (WCI)
		SELECT	s.Employee_ID
				, 59 Rate_ID
				, 'WCI' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		--
		UNION ALL
		--
		--RATE 60 (WCL)
		SELECT	s.Employee_ID
				, 60 Rate_ID
				, 'WCL' Rate_name
				, s.[EMP_TYPE_CD]
				, s.[HOURLY_RATE_NBR]
				, s.[MPFPT_Fringe]
				, 0 val
		FROM clean_src s
		;
		;
		--order by s.Employee_id, rate_id;