USE [$(MAIN_DB)]
GO


IF OBJECT_ID('vdot_vw_webims_staged_inventory') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_webims_staged_inventory]
GO


/****** Object:  View [dbo].[vdot_vw_webims_staged_inventory]    Script Date: 7/5/2017 3:42:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[vdot_vw_webims_staged_inventory] AS

WITH sub AS
(
SELECT s.stockcatlogid,
  s.stockno,
  mfr_id,	
  s.description name,
  CASE RIGHT(qty.mfr_id,1)
      WHEN 'N' THEN 'New'
      WHEN 'O' THEN 'Obsolete'
      WHEN 'S' THEN 'Surplus'
      WHEN 'U' THEN 'Used, Serviceable'
      WHEN 'X' THEN 'Used, Unserviceable'
     END AS CONDITION,
  s.unitsdesc,
  ISNULL(qty.stockqty, 0)              AS StockQTY,
  ISNULL(qty.unitcost, 0)              AS UnitCost,
  1                                    AS PermID
     FROM   $(STAGING_DB).dbo.deb_webims_today_stockcatalog s
     --left JOIN to include inventory that potentionally isn't in stock anywhere
     JOIN (SELECT SUM(ISNULL(stockqty, 0)) StockQTY,
         AVG(ISNULL(unitcost, 0)) AS UnitCost,
         stockcatalogid,
         stockno mfr_id
        FROM   $(STAGING_DB).dbo.deb_webims_today_inventory
        GROUP  BY stockcatalogid, stockno) qty
       ON s.stockcatlogid = qty.stockcatalogid
)
, sub2 AS
(
SELECT  name + ' (' + condition + ')' name
		, name + ' (' + condition + ')' description
		, sub.STOCKCATLOGID
		, sub.STOCKNO + RIGHT(sub.mfr_id,1) STOCKNO
		, CASE 
  			WHEN sub.mfr_id IS NULL THEN sub.STOCKNO
  			ELSE sub.mfr_id
  		  END mfr_id
		, sub.CONDITION
		, sub.UNITSDESC
		, sub.StockQTY
		, sub.UnitCost
		, sub.PermID
  FROM sub
),
sub3 AS 
(
	SELECT	CASE WHEN Condition = 'New' THEN CAST(sub2.STOCKCATLOGID AS VARCHAR(255))+'1'
				 WHEN Condition = 'Surplus' THEN CAST(sub2.STOCKCATLOGID AS VARCHAR(255))+'3'
				 WHEN Condition = 'Used, Serviceable' THEN CAST(sub2.STOCKCATLOGID AS VARCHAR(255))+'5'
				 WHEN Condition = 'Used, UnServiceable' THEN CAST(sub2.STOCKCATLOGID AS VARCHAR(255))+'7'
				 WHEN Condition = 'Obsolete' THEN CAST(sub2.STOCKCATLOGID AS VARCHAR(255))+'9'
			END uncastedId
			, sub2.* 
	FROM sub2
)
SELECT 
	CAST(sub3.uncastedId AS INT) uniqueInventoryWithConditionId,
	x.nigp_code,
		sub3.*
	
FROM sub3
LEFT JOIN dbo.vdot_vw_xref_nigp_webims x ON x.stock_catalog_id =sub3.stockcatlogid  ;


GO


--SELECT * FROM vdot_vw_webims_staged_inventory;