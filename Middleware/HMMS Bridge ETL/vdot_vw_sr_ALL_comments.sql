USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_vw_sr_ALL_comments') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_sr_ALL_comments]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vdot_vw_sr_ALL_comments] AS
WITH allSR AS (
SELECT 
	c.ID + ABS(CHECKSUM(NewId())) % 100000  as ID
	,i.Incident_ID AS HMMS_SR_ID
	,i.ref_id as CSC_SR_ID
	,NULL AS WorkOrderID
	,'Service Request' AS CommentType
	,c.Comment  AS Comment
	,c.CreationDate AS CommentCreationDate
	,c.UpdateDate AS CommentUpdateDate
FROM $(MAIN_DB).dbo.tbl_Comments C
JOIN $(MAIN_DB).dbo.tbl_Incident_Reports I ON I.Incident_ID = c.Ref_ID 
WHERE c.RefType = 0
and c.CreationDate > dateadd(dd,-5,GETUTCDATE())
AND i.ref_id IS NOT NULL

UNION ALL
SELECT 
	c.ID + ABS(CHECKSUM(NewId())) % 10000  as ID
	,i.Incident_ID
	,i.ref_id
	,m.Activity_ID
	,'Work Order' AS CommentType
	,c.Comment 
	,c.CreationDate
	,c.UpdateDate
FROM $(MAIN_DB).dbo.tbl_Comments C
JOIN $(MAIN_DB).dbo.tbl_Maintenance_Activity m ON m.Activity_ID = c.Ref_ID
LEFT JOIN tbl_Incident_Reports i ON i.Incident_ID = m.Incident_ID
WHERE c.RefType = 1
and c.CreationDate > dateadd(dd,-5,GETUTCDATE())
AND i.ref_id is not null
)

select * from allSR
where allSR.CSC_SR_ID COLLATE sql_latin1_general_cp1_ci_as IN
(
	select sr.vdot_srid 	
	from $(MSCRM).dbo.vdot_servicerequest sr
)

------duplicates
UNION ALL

select 
	allSR.ID+ ABS(CHECKSUM(NewId())) % 10000  as ID
	,dups.HMMS_SR_ID	
	,dups.CSC_SR_ID	
	,allSR.WorkOrderID	
	,allSR.CommentType	
	,allSR.Comment	
	,allSR.CommentCreationDate	
	,allSR.CommentUpdateDate
 from allSR
INNER JOIN [dbo].[vdot_vw_hmms_duplicates] as dups on allSR.HMMS_SR_ID = dups.parent_ID
where allSR.CSC_SR_ID COLLATE sql_latin1_general_cp1_ci_as IN
(
	select sr.vdot_srid 	
	from $(MSCRM).dbo.vdot_servicerequest sr
)

GO