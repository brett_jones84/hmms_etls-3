USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_vw_csc_SRs_No_WOID') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_csc_SRs_No_WOID]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE VIEW [dbo].[vdot_vw_csc_SRs_No_WOID] AS
SELECT top 100
	  SR.[vdot_servicerequestId]
	  ,SR.[vdot_srid] AS [CSC_SR_ID]   	  
	  ,SR.[vdot_HMMSIntegrationID] AS [HMMS_SR_ID]   
	  ,SR.vdot_HMMSWorkOrderID AS HMMS_WO_ID
	  , hmms_wo.WorkOrderID
	  , hmms_wo.ID AS HMMS_WO_DBID
  FROM [$(MSCRM)].[dbo].[vdot_servicerequest] SR with (NOLOCK)
  left join   
  		  (
		select 	  
				wo.WorkOrderID
			  , wo.ID  
			  ,wo.ParentServiceRequestID
		from $(MAIN_DB).reports.WorkOrders  as wo

		UNION ALL 

		select wo.WorkOrderID , wo.id , dupes.HMMS_SR_ID from $(MAIN_DB).dbo.vdot_vw_hmms_duplicates as dupes
		left join $(MAIN_DB).reports.WorkOrders as wo on dupes.parent_ID = wo.ParentServiceRequestID
		)   
  as hmms_wo on SR.vdot_HMMSIntegrationID = hmms_wo.ParentServiceRequestID
  where sr.[vdot_srid] is not null
  and hmms_wo.WorkOrderID is not null
  and SR.vdot_HMMSWorkOrderID is null
    and SR.[vdot_SubmittedOn] > dateadd( dd,-5, getdate())
	and SR.vdot_HMMSIntegrationID is not null
	and ( (SR.[vdot_ServiceRequestType] = 866190000 -- "MAINTENANCE"
			and SR.[statuscode] =1 -- "Assigned"
			)
			or 
		 (SR.vdot_ServiceRequestType = 866190002 -- "Weather" 
		  AND SR.vdot_DeploymentStatus in (866190001, 866190002)) -- No/Callout or Mixed)
		  AND SR.vdot_notifytoc2 = 866190000		  
		)





GO