USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_vw_hmms_duplicates') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_hmms_duplicates]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[vdot_vw_hmms_duplicates] AS
SELECT   
	dups.Incident_ID AS HMMS_SR_ID
	, sr.RefID as CSC_SR_ID
	, sr.Status
	, sr.StatusName
	, parents.Incident_ID AS parent_ID
	
FROM     dbo.tbl_Incident_Reports_Duplicate AS dups 
LEFT JOIN
                 (SELECT   ID, Instance_ID, Incident_ID, IsPrimary, IsIgnored
                 FROM     dbo.tbl_Incident_Reports_Duplicate
                 WHERE   (IsPrimary = 1)) AS parents ON dups.Instance_ID = parents.Instance_ID
LEFT JOIN $(MAIN_DB).reports.ServiceRequests as sr on dups.Incident_ID = sr.ID

WHERE   (dups.IsPrimary = 0)




GO