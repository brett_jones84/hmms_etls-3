USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_vw_csc_all_SRs') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_csc_all_SRs]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vdot_vw_csc_all_SRs] AS
SELECT SR.[CreatedByName], SR.[vdot_JurisdictionName]
      ,ISNULL(SR.[vdot_CustomerName],'') [vdot_CustomerName]
      ,SR.[vdot_DistrictName], SR.[vdot_AreaHQName]
	  ,SR.[vdot_AssetTypeName], SR.[vdot_SubmittedOn]
      ,ISNULL(SR.[vdot_CustomerYomiName],'') [vdot_CustomerYomiName]
      ,SR.[vdot_ResidencyName]
	  ,CASE 
			WHEN SR.vdot_ServiceRequestType = 866190002
			THEN 'Other'
			ELSE SR.vdot_ProblemTypeName 
		END vdot_ProblemTypeName
	  ,SR.[OwnerIdName],SR.[OwnerIdDsc],SR.[vdot_servicerequestId]
      ,SR.[CreatedOn], SR.[ModifiedOn], SR.[vdot_srid]   
	  ,SR.[vdot_HMMSIntegrationID]
	  ,SR.[vdot_HMMSWorkOrderID]
      ,SR.[vdot_Location], SR.[vdot_AMSID]
      ,ISNULL(SR.[vdot_Lat], 0) [vdot_Lat]
      ,ISNULL(SR.[vdot_Lon], 0) [vdot_Lon]
      ,SR.[vdot_RouteNumber]
	  ,SR.[vdot_Comments]
      ,SR.[vdot_Animal],SR.[vdot_Located],SR.[vdot_Phone1]
      --, (CASE WHEN contact.vdot_declinedemail=1 THEN 'Declined' ELSE SR.vdot_Email END )  [vdot_Email]
	  ,SR.vdot_Email [vdot_Email]
      ,SR.[vdot_Phone1Extension]
      ,SR.[vdot_Address]
      ,SR.[vdot_City]
	,Case SR.[vdot_ServiceAreaType] 
		when 866190000 then 1 --'Interstate'
		when 866190001 then 2 --'Primary'		
		when 866190002 then 3 --'Secondary'		
		else '' end [vdot_ServiceArea],
	'' [vdot_WorkArea]--SR.[vdot_WorkArea]
      ,ISNULL(SR.[vdot_MileTo],0) [vdot_MileTo]
      ,ISNULL(SR.[vdot_MileFrom],0) [vdot_MileFrom]
	  ,district.vdot_Code [district_AMSCode]
	  ,CASE 
			WHEN SR.vdot_ServiceRequestType = 866190002
			THEN 22
			ELSE ISNULL(problemtype.vdot_AMSCode, '0') 
		END [problemtype_AMSCode]
	  ,ISNULL(contact.FirstName,'') [Customer_FirstName]
	  ,ISNULL(contact.LastName,'') [Customer_LastName]
	  ,ISNULL(contact.EMailAddress1,'') [Customer_EmailAddress1]
	  ,ISNULL(contact.Telephone1,'') [Customer_Telephone1]
	  ,ISNULL(contact.Address1_Composite,'') [Customer_Address1_Composite]
	  , CASE 
			WHEN SR.vdot_ServiceRequestType = 866190002
			THEN 400 --Flexible Pavement
			ELSE assetType.vdot_AMSCode 
		END [AssetType_AMSCode]
	  ,j.vdot_AMSID [Jurisdiction_AmsId]
	  ,(CASE 
		  WHEN ((cast(j.vdot_JurisdictionCode as int)) >= 300) THEN j.vdot_AMSID
		  ELSE 0
		END) [vdot_juridiction_city]
	  ,(CASE 
		  WHEN ((cast(j.vdot_JurisdictionCode as int)) >= 300) THEN 0
		  ELSE j.vdot_AMSID
		END) [vdot_juridiction_county],
		isnull(case when charindex('|',wa.vdot_AMSWorkAreaID)=0 then '' else wa.vdot_AMSWorkAreaID end,'') [vdot_AMSWorkAreaID]

		,isnull(case when wa.vdot_AMSWorkAreaID is null then j.[vdot_AMSID]+'_'+SR.[vdot_JurisdictionName] else wa.[vdot_name] end,'') [vdot_AMSWorkAreaName]

	  ,ISNULL(SR.[vdot_Phone2],'') [vdot_Phone2]
  FROM [$(MSCRM)].[dbo].[vdot_servicerequest] SR with (NOLOCK)
  inner join [$(MSCRM)].[dbo].[vdot_district] district with (NOLOCK) on SR.vdot_District = district.vdot_districtId
  inner join [$(MSCRM)].[dbo].[vdot_problemtype] problemtype with (NOLOCK) on SR.vdot_ProblemType = problemtype.vdot_problemtypeId
  left join [$(MSCRM)].[dbo].[Contact] contact with (NOLOCK) on SR.vdot_Customer = contact.ContactId
  left join [$(MSCRM)].[dbo].vdot_assettype assetType with (NOLOCK) on SR.vdot_AssetType = assetType.vdot_assettypeId
  inner join [$(MSCRM)].[dbo].vdot_jurisdiction j with (NOLOCK) on sr.vdot_Jurisdiction = j.vdot_jurisdictionId
  left outer join [$(MSCRM)].[dbo].vdot_workarea wa with (NOLOCK) on sr.vdot_AMSWorkArea = wa.vdot_workareaID
  where 
	sr.[vdot_srid] is not null
    --and SR.[vdot_SubmittedOn] > dateadd( dd,-4, getdate())
	--and isnull(SR.[vdot_AMSID],-1) = -1
	and ( (SR.[vdot_ServiceRequestType] = 866190000 -- "MAINTENANCE"
			and SR.[statuscode] =1 -- "Assigned"
			)
			or 
		 (SR.vdot_ServiceRequestType = 866190002 -- "Weather" 
		  AND SR.vdot_DeploymentStatus in (866190001, 866190002)) -- No/Callout or Mixed)
		  AND SR.vdot_notifytoc2 = 866190000
		  
		)






GO