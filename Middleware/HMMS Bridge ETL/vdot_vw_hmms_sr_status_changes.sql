USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_vw_hmms_sr_status_changes') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_hmms_sr_status_changes]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE VIEW [dbo].[vdot_vw_hmms_sr_status_changes] AS

WITH src as (
SELECT        
	hmms_sr.ID
	, hmms_sr.RefID
	, hmms_sr.ServiceRequestID	
	, wo.StatusName AS woStatus
	, wo_mf.[Assigned/Pending]
	, COALESCE(
		CASE WHEN wo.StatusName IN ('Open','Hold') THEN NULLIF(wo_mf.[Assigned/Pending],'') ELSE NULL END,
		CASE WHEN wo.StatusName = 'Closed' THEN wo.StatusName ELSE NULL END,
		'Assigned') AS StatusName
FROM   
    [$(MAIN_DB)].[reports].[ServiceRequests] as hmms_sr
	INNER JOIN [$(MAIN_DB)].[reports].[WorkOrders] as wo on hmms_sr.ID = wo.ParentServiceRequestID
	LEFT JOIN [$(MAIN_DB)].[reports].WorkOrderForm_MaintenanceWorkOrderForm  as wo_mf on wo.id = wo_mf.id
)

SELECT 
	src.ID as HMMS_SR_ID
	, src.RefID as CSC_SR_ID
	, src.ServiceRequestID
	, src.woStatus	
	, src.StatusName as HMMS_STATUS
	, sc_lu.CSC_Status_Id as HMMS_STATUS_ID
	, csc_sr.statuscode as CSC_STATUS
	
	
from src
	LEFT JOIN [$(MAIN_DB)].[dbo].[vdot_csc_statusCode_lookup] as sc_lu ON LOWER(src.StatusName) = sc_lu.StatusName
	-- inner JOIN [$(MSCRM)].[dbo].[vdot_servicerequest] as csc_sr on src.ID = csc_sr.vdot_HMMSIntegrationID
	inner JOIN [CSC_MSCRM].[dbo].[vdot_servicerequest] as csc_sr on src.RefID collate Latin1_General_CI_AI = csc_sr.vdot_srid
where 
	src.RefID != '' AND sc_lu.CSC_Status_Id != csc_sr.statuscode AND csc_sr.statuscode != 866190003 AND csc_sr.statuscode !=866190008


--Duplicate section
UNION ALL

SELECT 
	dups.HMMS_SR_ID as HMMS_SR_ID
	, dups.CSC_SR_ID as CSC_SR_ID
	, src.ServiceRequestID
	, src.woStatus	
	, src.StatusName as HMMS_STATUS
	, sc_lu.CSC_Status_Id as HMMS_STATUS_ID
	, csc_sr.statuscode as CSC_STATUS
	
	
from src
	LEFT JOIN [$(MAIN_DB)].[dbo].[vdot_csc_statusCode_lookup] as sc_lu ON LOWER(src.StatusName) = sc_lu.StatusName
	inner JOIN [$(MSCRM)].[dbo].[vdot_servicerequest] as csc_sr on src.ID = csc_sr.vdot_HMMSIntegrationID
	INNER JOIN [dbo].[vdot_vw_hmms_duplicates] as dups on src.id = dups.parent_ID
where 
	src.RefID != '' AND sc_lu.CSC_Status_Id != csc_sr.statuscode AND csc_sr.statuscode != 866190003 AND csc_sr.statuscode !=866190008





GO