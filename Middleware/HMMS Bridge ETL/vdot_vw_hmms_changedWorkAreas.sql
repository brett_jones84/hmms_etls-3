USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_vw_hmms_changedWorkAreas') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_hmms_changedWorkAreas]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO













CREATE VIEW [dbo].[vdot_vw_hmms_changedWorkAreas] AS
SELECT
        hmms_sr.ID AS HMMS_SR_ID
		, csc_sr.vdot_srid  AS CSC_SR_ID		
		,hmms_sr.AssignedToName as Workareacurrentassignment		
		, csc_wa.vdot_AMSWorkAreaID
		, csc_wa.vdot_name		
FROM
        reports.ServiceRequests AS hmms_sr 
			INNER JOIN
                $(MSCRM).dbo.vdot_servicerequest AS csc_sr with (NOLOCK)
				ON 
				hmms_sr.RefID = csc_sr.vdot_srid COLLATE SQL_Latin1_General_CP1_CI_AS 
				
			LEFT OUTER JOIN
                $(MSCRM).dbo.vdot_workarea AS csc_wa with (NOLOCK)
				ON 
				csc_sr.vdot_AMSWorkArea = csc_wa.vdot_workareaId
			
WHERE
	hmms_sr.AssignedToName != 'Unassigned' AND hmms_sr.AssignedToName != ISNULL(csc_wa.vdot_name,'')	COLLATE SQL_Latin1_General_CP1_CI_AS and hmms_sr.AssignedToName is not null

	--Get duplicates and promote the values...

	union all

	SELECT
        dups.HMMS_SR_ID AS HMMS_SR_ID
		, dups.CSC_SR_ID COLLATE SQL_Latin1_General_CP1_CI_AS AS CSC_SR_ID		
		,hmms_sr.AssignedToName as Workareacurrentassignment		
		, csc_wa.vdot_AMSWorkAreaID
		, csc_wa.vdot_name
		
FROM
        reports.ServiceRequests AS hmms_sr 
			INNER JOIN
                $(MSCRM).dbo.vdot_servicerequest AS csc_sr with (NOLOCK)
				ON 
				hmms_sr.RefID = csc_sr.vdot_srid COLLATE SQL_Latin1_General_CP1_CI_AS 
				
			LEFT OUTER JOIN
                $(MSCRM).dbo.vdot_workarea AS csc_wa with (NOLOCK)
				ON 
				csc_sr.vdot_AMSWorkArea = csc_wa.vdot_workareaId

				INNER JOIN [dbo].[vdot_vw_hmms_duplicates] as dups on hmms_sr.id = dups.parent_ID
			
WHERE
	hmms_sr.AssignedToName != 'Unassigned' AND hmms_sr.AssignedToName != ISNULL(csc_wa.vdot_name,'')	COLLATE SQL_Latin1_General_CP1_CI_AS and hmms_sr.AssignedToName is not null

	





GO