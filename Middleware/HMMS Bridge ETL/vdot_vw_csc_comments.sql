USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_vw_csc_comments') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_csc_comments]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
















CREATE VIEW [dbo].[vdot_vw_csc_comments] AS 
  WITH src 
		--added a.AnnotationId -mhk
       AS (SELECT a.AnnotationId,
				  sr.vdot_srid, 
                  --sr.vdot_amsid, 
				  sr.vdot_HMMSIntegrationID,
                  a.subject, 
                  a.objecttypecode, 
                  a.isdocument, 
                  a.modifiedon, 
                  --  replace(replace(replace(replace(replace(a.NoteText,'"','"'),'''','''),'&','&'),'>','>'),'<','<') NoteText,
                                    --a.notetext 
				   --get rid of all whitespace at the beginning and end of the string - 
				--HMMS does this when it stores comments and CSC does not so we need to true it up here.             
				   RTRIM(
						LTRIM(
							RIGHT(
								LEFT(NoteText,LEN(NoteText)-PATINDEX('%[^'+CHAR(13)+CHAR(10)+']%',REVERSE(NoteText))+1)
							,LEN(
								LEFT(NoteText,LEN(NoteText)-PATINDEX('%[^'+CHAR(13)+CHAR(10)+']%',REVERSE(NoteText))+1)
							)-PATINDEX('%[^'+CHAR(13)+CHAR(10)+']%',
								LEFT(NoteText,LEN(NoteText)-PATINDEX('%[^'+CHAR(13)+CHAR(10)+']%',REVERSE(NoteText))+1))+1)
						)
					) as NoteText
           -- If CDDATA is not working revert to the above Replace commands. 
           FROM   $(MSCRM).dbo.annotation a with (NOLOCK)
                  INNER JOIN $(MSCRM).dbo.vdot_servicerequest sr 
                          ON a.objectid = sr.vdot_servicerequestid 
           WHERE  a.modifiedon > Dateadd(dd, -5, Getutcdate()) 
                  --AND sr.vdot_amsid IS NOT NULL 
				  AND sr.vdot_HMMSIntegrationID IS NOT NULL 
           UNION 
		   --added a.AnnotationId -mhk
           SELECT sr.vdot_servicerequestId as [AnnotationId],
				  sr.vdot_srid, 
                  --sr.vdot_amsid, 
				  sr.vdot_HMMSIntegrationID, 
                  'Comment from AMS' AS [Subject], 
                  10020              AS [ObjectTypeCode], 
                  0                  AS [IsDocument], 
                  NULL, 
                  --  replace(replace(replace(replace(replace(a.NoteText,'"','"'),'''','''),'&','&'),'>','>'),'<','<') NoteText,
                                    --sr.vdot_comments   AS [NoteText] 

				   --get rid of all whitespace at the beginning and end of the string - 
				--HMMS does this when it stores comments and CSC does not so we need to true it up here.             
				   RTRIM(
						LTRIM(
							RIGHT(
								LEFT(vdot_comments,LEN(vdot_comments)-PATINDEX('%[^'+CHAR(13)+CHAR(10)+']%',REVERSE(vdot_comments))+1)
							,LEN(
								LEFT(vdot_comments,LEN(vdot_comments)-PATINDEX('%[^'+CHAR(13)+CHAR(10)+']%',REVERSE(vdot_comments))+1)
							)-PATINDEX('%[^'+CHAR(13)+CHAR(10)+']%',
								LEFT(vdot_comments,LEN(vdot_comments)-PATINDEX('%[^'+CHAR(13)+CHAR(10)+']%',REVERSE(vdot_comments))+1))+1)
						)
					) as NoteText
           -- If CDDATA is not working revert to the above Replace commands. 
           FROM   $(MSCRM).dbo.vdot_servicerequest sr with (NOLOCK)
           WHERE  sr.modifiedon > Dateadd(dd, -5, Getutcdate()) 
                  --AND sr.vdot_amsid IS NOT NULL), 
				  AND sr.vdot_HMMSIntegrationID IS NOT NULL), 
       csc 
	   --added a.AnnotationId -mhk
       AS (SELECT AnnotationId,
				  vdot_srid, 
                  --vdot_amsid, 
				  vdot_HMMSIntegrationID, 
                  subject, 
                  notetext, 
                  Min(modifiedon) modifiedOn, 
                  Count(*)        duplicateCount 
           FROM   src 
		   --added a.AnnotationId -mhk
           GROUP  BY AnnotationId,
					 vdot_srid, 
                     --vdot_amsid, 
					 vdot_HMMSIntegrationID,
                     subject, 
                     notetext) 
  SELECT AnnotationId
  ,vdot_srid as CSC_SR_ID
  --,vdot_amsid as HMMS_SR_ID
  ,vdot_HMMSIntegrationID as HMMS_SR_ID
  ,subject
  ,NoteText
  ,modifiedOn
  ,duplicateCount
  FROM   csc 
         LEFT JOIN [$(MAIN_DB)].[dbo].[vdot_vw_sr_ALL_comments] vw 
                ON csc.vdot_srid = vw.csc_sr_id COLLATE 
                                   sql_latin1_general_cp1_ci_as 
                   AND csc.notetext = vw.comment COLLATE 
                                      sql_latin1_general_cp1_ci_as 
		INNER JOIN [$(MAIN_DB)].reports.ServiceRequests as hmms_sr 
				on hmms_sr.ID = csc.vdot_HMMSIntegrationID COLLATE 
                                      sql_latin1_general_cp1_ci_as
					AND 
					hmms_sr.RefID = csc.vdot_srid COLLATE 
                                      sql_latin1_general_cp1_ci_as
  WHERE  csc.notetext IS NOT NULL 
         AND vw.comment IS NULL 
		 ----remove to enable..
		 --and vw.comment = '1234567890lkadjsf'
		 AND ISNULL(csc.Subject,'') not in ('Comment from AMS','Cancellation Reason', 'Closed Status Reason (To Customer)','Open Status Reason (To Customer)','Response to Customer: (Optional)')

  

; 














GO


