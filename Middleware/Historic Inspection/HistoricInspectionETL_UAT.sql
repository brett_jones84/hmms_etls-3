-- This process migrates historical ancillary structures from the HMMS_STAGING database to the HMMS database in UAT. 
-- Note that the job is a full wipe/reload currently and can be run multiple times.

-- Database Server Name:   Wsq01443

-- Database Names:         HMMS  
--                         HMMS_STAGING

-- The following steps will create the necessary objects for the Historical Ancillary Structures import and run the load. 
-- This script will drop the following tables from HMMS, recreate them, then populate them with data from HMMS_STAGING.

--       --ANCILLARY_STRUCTURE_INSPECTION
--       --ANCILLARY_STRUCTURE_TYPES


-- Step 1
--     Take a backup of the database

-- Step 2: Ensure person executing script has the following permissions in the target databases:

-- 	a) create a table in the HMMS database
-- 	b) edit table data in HMMS and HMMS_STAGING
-- 	c) read data from HMMS_STAGING database (for tables)

-- Step 3:
--     Execute the Create_and_Populate_Historical_Ancillary_Structure_Tables.sql in SQL Server Manangment Studio
--     on server Wsq01443.

-- RollBack Plan
--     In case of failure restore the database to the backup created in step 1



-- SQL to execute:

USE [HMMS]
GO
/****** Object:  Table 
				 Table [dbo].[ANCILLARY_STRUCTURE_INSPECTION]			 
				 Table [dbo].[ANCILLARY_STRUCTURE_TYPES]  

-- Author: Michael Kolonay (WorldView Solutions)
-- Script Date: 7/11/2017 3:44:11 PM 
--
-- This script creates and populates the 

--ANCILLARY_STRUCTURE_INSPECTION
--ANCILLARY_STRUCTURE_TYPES

 tables in the HMMS database.  
 From Data in the HMMS_STAGING database.

 all datefields that are stored as varchar are transformed to datetime
******/

SET ANSI_NULLS ON
GO

--speed things up some with this setting
SET NOCOUNT ON
GO
--*************************************************************************

IF OBJECT_ID('ANCILLARY_STRUCTURE_INSPECTION') IS NOT NULL
	DROP TABLE  [dbo].[ANCILLARY_STRUCTURE_INSPECTION]
GO

SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [dbo].[ANCILLARY_STRUCTURE_INSPECTION](
	[STRUCTURE_NUMBER] [varchar](7) NOT NULL,
	[STRUCTURE_IDENTIFIER] [varchar](15) NOT NULL,
	[INSPECTION_DATE] [datetime] NOT NULL,
	[INSPECTION_TYPE] [decimal](2, 0) NULL,
	[INSPECTION_TYPE_DESC] [varchar](60) NULL,
	[LEAD_INSPECTOR_CODE] [varchar](4) NULL,
	[LEAD_INSPECTOR_NAME] [varchar](60) NULL,
	[ADDITIONAL_INSPECTOR_CODE] [varchar](4) NULL,
	[ADDITIONAL_INSPECTOR_NAME] [varchar](60) NULL,
	[LOCATION_DESCRIPTION] [varchar](500) NULL,
	[NODE_DESCRIPTION] [varchar](500) NULL,
	[SIGN_TEXT_COMMENTS] [varchar](500) NULL,
	[OVERALL_STRUC_COMMENTS] [varchar](500) NULL,
	[WORKPERFORMED_COMMENTS] [varchar](500) NULL,
	[CLEARANCE_COMMENTS] [varchar](500) NULL,
	[UNDER_CLEARANCE_SHEET] [varchar](1) NULL,
	[SKETCHES_SHEET] [varchar](1) NULL,
	[LOCATION_SHEET] [varchar](1) NULL,
	[OTHER_SHEETS] [varchar](1) NULL,
	[CLEAR_UNDER_FLS] [decimal](3, 1) NULL,
	[LANE_CLEARANCE_1] [decimal](3, 1) NULL,
	[LANE_CLEARANCE_2] [decimal](3, 1) NULL,
	[LANE_CLEARANCE_3] [decimal](3, 1) NULL,
	[LANE_CLEARANCE_4] [decimal](3, 1) NULL,
	[LANE_CLEARANCE_5] [decimal](3, 1) NULL,
	[LANE_CLEARANCE_6] [decimal](3, 1) NULL,
	[LANE_CLEARANCE_7] [decimal](3, 1) NULL,
	[LANE_CLEARANCE_8] [decimal](3, 1) NULL,
	[CLEAR_UNDER_LCS] [decimal](3, 1) NULL,
	[CLEAR_UNDER_FRS] [decimal](3, 1) NULL,
	[CLEAR_UNDER_RCS] [decimal](3, 1) NULL,
	[VERTICAL_CLEARANCE] [decimal](3, 1) NULL,
	[LAST_MOD_USER] [varchar](60) NULL,
	[LAST_MOD_DATE] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[STRUCTURE_NUMBER] ASC,
	[STRUCTURE_IDENTIFIER] ASC,
	[INSPECTION_DATE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

INSERT INTO dbo.ANCILLARY_STRUCTURE_INSPECTION(	
		[STRUCTURE_NUMBER]
      ,[STRUCTURE_IDENTIFIER]
      ,[INSPECTION_DATE]
      ,[INSPECTION_TYPE]
      ,[INSPECTION_TYPE_DESC]
      ,[LEAD_INSPECTOR_CODE]
      ,[LEAD_INSPECTOR_NAME]
      ,[ADDITIONAL_INSPECTOR_CODE]
      ,[ADDITIONAL_INSPECTOR_NAME]
      ,[LOCATION_DESCRIPTION]
      ,[NODE_DESCRIPTION]
      ,[SIGN_TEXT_COMMENTS]
      ,[OVERALL_STRUC_COMMENTS]
      ,[WORKPERFORMED_COMMENTS]
      ,[CLEARANCE_COMMENTS]
      ,[UNDER_CLEARANCE_SHEET]
      ,[SKETCHES_SHEET]
      ,[LOCATION_SHEET]
      ,[OTHER_SHEETS]
      ,[CLEAR_UNDER_FLS]
      ,[LANE_CLEARANCE_1]
      ,[LANE_CLEARANCE_2]
      ,[LANE_CLEARANCE_3]
      ,[LANE_CLEARANCE_4]
      ,[LANE_CLEARANCE_5]
      ,[LANE_CLEARANCE_6]
      ,[LANE_CLEARANCE_7]
      ,[LANE_CLEARANCE_8]
      ,[CLEAR_UNDER_LCS]
      ,[CLEAR_UNDER_FRS]
      ,[CLEAR_UNDER_RCS]
      ,[VERTICAL_CLEARANCE]
      ,[LAST_MOD_USER]
      ,[LAST_MOD_DATE]
	)
   SELECT 
   [STRUCTURE_NUMBER]
      ,[STRUCTURE_IDENTIFIER]
      ,[INSPECTION_DATE]
      ,[INSPECTION_TYPE]
      ,[INSPECTION_TYPE_DESC]
      ,[LEAD_INSPECTOR_CODE]
      ,[LEAD_INSPECTOR_NAME]
      ,[ADDITIONAL_INSPECTOR_CODE]
      ,[ADDITIONAL_INSPECTOR_NAME]
      ,[LOCATION_DESCRIPTION]
      ,[NODE_DESCRIPTION]
      ,[SIGN_TEXT_COMMENTS]
      ,[OVERALL_STRUC_COMMENTS]
      ,[WORKPERFORMED_COMMENTS]
      ,[CLEARANCE_COMMENTS]
      ,[UNDER_CLEARANCE_SHEET]
      ,[SKETCHES_SHEET]
      ,[LOCATION_SHEET]
      ,[OTHER_SHEETS]
      ,[CLEAR_UNDER_FLS]
      ,[LANE_CLEARANCE_1]
      ,[LANE_CLEARANCE_2]
      ,[LANE_CLEARANCE_3]
      ,[LANE_CLEARANCE_4]
      ,[LANE_CLEARANCE_5]
      ,[LANE_CLEARANCE_6]
      ,[LANE_CLEARANCE_7]
      ,[LANE_CLEARANCE_8]
      ,[CLEAR_UNDER_LCS]
      ,[CLEAR_UNDER_FRS]
      ,[CLEAR_UNDER_RCS]
      ,[VERTICAL_CLEARANCE]
      ,[LAST_MOD_USER]
      ,[LAST_MOD_DATE]
	FROM [HMMS_STAGING].[dbo].[ANCILLARY_STRUCTURE_INSPECTION]

GO


--************************************************************************************************


IF OBJECT_ID('ANCILLARY_STRUCTURE_TYPES') IS NOT NULL
	DROP TABLE  [dbo].[ANCILLARY_STRUCTURE_TYPES]
	SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [dbo].[ANCILLARY_STRUCTURE_TYPES](
	[STRUCTURE_IDENTIFIER] [varchar](15) NOT NULL,
	[STRUCTURE_ELEMENT_TYPE] [varchar](60) NOT NULL,
	[INSPECTION_DATE] [datetime2](7) NOT NULL,
	[STRUCTURE_ELEMENT_NAME] [varchar](100) NOT NULL,
	[STRUCTURE_ELEMENT_RATING] [varchar](50) NULL,
	[STRUC_ELEMENT_RATING_DESC] [varchar](500) NULL,
	[STRUCTURE_ELEMENT_COMMENTS] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[STRUCTURE_IDENTIFIER] ASC,
	[STRUCTURE_ELEMENT_TYPE] ASC,
	[INSPECTION_DATE] ASC,
	[STRUCTURE_ELEMENT_NAME] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

INSERT INTO dbo.ANCILLARY_STRUCTURE_TYPES(	
[STRUCTURE_IDENTIFIER]
      ,[STRUCTURE_ELEMENT_TYPE]
      ,[INSPECTION_DATE]
      ,[STRUCTURE_ELEMENT_NAME]
      ,[STRUCTURE_ELEMENT_RATING]
      ,[STRUC_ELEMENT_RATING_DESC]
      ,[STRUCTURE_ELEMENT_COMMENTS]
	)
   SELECT 
[STRUCTURE_IDENTIFIER]
      ,[STRUCTURE_ELEMENT_TYPE]
      ,[INSPECTION_DATE]
      ,[STRUCTURE_ELEMENT_NAME]
      ,[STRUCTURE_ELEMENT_RATING]
      ,[STRUC_ELEMENT_RATING_DESC]
      ,[STRUCTURE_ELEMENT_COMMENTS]
	FROM [HMMS_STAGING].[dbo].[ANCILLARY_STRUCTURE_TYPES]

GO


--Get only the most recent data for each structure ID

-- select ST.STRUCTURE_IDENTIFIER, count(ST.STRUCTURE_IDENTIFIER) as countSTRUCTURE_IDENTIFIER from (
--   SELECT t1.*
-- FROM [HMMS_Staging].[dbo].[ANCILLARY_STRUCTURE_TYPES] t1 LEFT JOIN [HMMS_Staging].[dbo].[ANCILLARY_STRUCTURE_TYPES] t2
-- ON (t1.STRUCTURE_IDENTIFIER = t2.STRUCTURE_IDENTIFIER AND t1.INSPECTION_DATE < t2.INSPECTION_DATE)
-- WHERE t2.INSPECTION_DATE IS NULL) as ST
-- group by STRUCTURE_IDENTIFIER
-- order by countSTRUCTURE_IDENTIFIER asc
